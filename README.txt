DESCRIPTION
-----------
Provides a migration process plugin that converts absolute links to relative.

A list of 'base urls' may be provided as parameters and any absolute links that start with these base urls are converted to relative links.

CONFIGURATION
-------------
Install the module, then add the 'filter_absolute_links' plugin to your migration Yaml file as shown in the following example:

plugin: filter_absolute_links
replace_urls:
    http://www.mysitename.co.uk
    https://www.mysitename.co.uk
    http://test-sitename.net

This would most commonly be used to filter absolute links out of a body field.
In the example above, a link such as https://www.mysitename.co.uk/news would be converted to /news during the migration.
An 'external' link such as http://bbc.co.uk/news would be left untouched by the migration.
